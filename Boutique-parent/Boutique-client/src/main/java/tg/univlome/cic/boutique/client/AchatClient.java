package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Achat;

/**
 *
 * @class AchatClient
 * 
 * @author Josué K.M. AYIVOR
 */

public class AchatClient implements serializable {
    
    private static AchatClient INSTANCE;
    private Achat achat = new Achat();;
    private List<Achat> liste = new ArrayList<>();;
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/achat";
    
    public static synchronized AchatClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AchatClient();
        }
        
        return INSTANCE;
    }
    
    public List<Achat> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<Achat>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public Achat trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (Achat ach : liste) {
                if (Objects.equals(ach.getId(), id_prod)) {
                    achat = ach;
                    return achat;
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        
        Response response = client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(Achat ach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(ach, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(Achat ach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(ach, MediaType.APPLICATION_JSON_TYPE));
                
        if (response.getStatus() == 200) {
            System.out.println("Achat modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
        
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        
        if (response.getStatus() == 200) {
            System.out.println("Achat supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }

}
