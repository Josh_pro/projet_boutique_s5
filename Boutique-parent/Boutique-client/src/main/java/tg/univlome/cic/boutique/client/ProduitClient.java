package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @class ProduitClient
 * 
 * @author Josué K.M. AYIVOR
 */

public class ProduitClient implements serializable {
    
    private static ProduitClient INSTANCE;
    private Produit produit = new Produit();;
    private List<Produit> liste = new ArrayList<>();;
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/produit";
    
    public static synchronized ProduitClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProduitClient();
        }
        return INSTANCE;
    }
    
    public List<Produit> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<Produit>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public Produit trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (Produit prod : liste) {
                if (prod.getId() == id_prod) {
                    produit = prod;
                    return produit;
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        Client $client = ClientBuilder.newClient();
        
        Response response = $client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(Produit prod) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(prod, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(Produit prod) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(prod, MediaType.APPLICATION_JSON_TYPE));
        if (response.getStatus() == 200) {
            System.out.println("Produit modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        if (response.getStatus() == 200) {
            System.out.println("Produit supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }
    
}
