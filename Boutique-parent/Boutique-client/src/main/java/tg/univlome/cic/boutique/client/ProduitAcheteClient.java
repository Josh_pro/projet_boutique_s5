package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @class ProduitAcheteClient
 * 
 * @author Josué K.M. AYIVOR
 */

public class ProduitAcheteClient implements serializable{
    private static ProduitAcheteClient INSTANCE;
    private ProduitAchete pdA = new ProduitAchete();
    private List<ProduitAchete> liste = new ArrayList<>();
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/ProduitAchete";
    
    public static synchronized ProduitAcheteClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProduitAcheteClient();
        }
        return INSTANCE;
    }
    
    public List<ProduitAchete> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<ProduitAchete>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public ProduitAchete trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (ProduitAchete pda : liste) {
                if (Objects.equals(pda.getId(), id_prod)) {
                    pdA = pda;
                    return pdA;
                } else {
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        
        Response response = client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(ProduitAchete Pach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(Pach, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(ProduitAchete Pach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(Pach, MediaType.APPLICATION_JSON_TYPE));
                
        if (response.getStatus() == 200) {
            System.out.println("Donnée produitAchete modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
        
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        
        if (response.getStatus() == 200) {
            System.out.println("ProduitAchete supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }
}
