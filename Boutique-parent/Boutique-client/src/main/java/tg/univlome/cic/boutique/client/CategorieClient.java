package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Achat;
import tg.univlome.cic.boutique.entites.Categorie;

/**
 *
 * @Class CategorieClient
 * 
 * @author Josué K.M. AYIVOR
 * 
 */

public class CategorieClient import serializable {
    
    private static CategorieClient INSTANCE;
    private Categorie categorie = new Categorie();;
    private List<Categorie> liste = new ArrayList<>();
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/categorie";
    
    public static synchronized CategorieClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategorieClient();
        }
        
        return INSTANCE;
    }
    
    public List<Categorie> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<Categorie>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public Categorie trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (Categorie cat : liste) {
                if (Objects.equals(cat.getId(), id_prod)) {
                    categorie = cat;
                    return categorie;
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        
        Response response = client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(Achat ach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(ach, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(Achat ach) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(ach, MediaType.APPLICATION_JSON_TYPE));
                
        if (response.getStatus() == 200) {
            System.out.println("Categorie modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
        
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        
        if (response.getStatus() == 200) {
            System.out.println("Categorie supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }
}
