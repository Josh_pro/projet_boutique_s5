package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Clients;

/**
 * @class Client 
 * 
 * @author Josué K.M. AYIVOR
 */

public class ClientClient implements serializable {
    
    private static ClientClient INSTANCE;
    private Clients clients = new Clients();
    private List<Clients> liste = new ArrayList<>();
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/client";
    
    public static synchronized ClientClient getInstance(){
        if(INSTANCE == null) {
            INSTANCE = new ClientClient();
        }
        return INSTANCE;
    }
    
    public List<Clients> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<Clients>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public Clients trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (Clients cli : liste) {
                if (Objects.equals(cli.getId(), id_prod)) {
                    clients = cli;
                    return clients;
                } else {
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        
        Response response = client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(Clients cli) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(cli, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(Clients cli) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(cli, MediaType.APPLICATION_JSON_TYPE));
                
        if (response.getStatus() == 200) {
            System.out.println("Donnée Client modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
        
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        
        if (response.getStatus() == 200) {
            System.out.println("client supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }

}
