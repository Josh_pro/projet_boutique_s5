package tg.univlome.cic.boutique.client;

import java.io.serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Employe;

/**
 *
 * @class EmployeClient
 * 
 * @author Josué K.M. AYIVOR
 */

public class EmployeClient implements serializable {

    private static EmployeClient INSTANCE;
    private Employe employe = new Employe();
    private List<Employe> liste = new ArrayList<>();
    private static final Client client = ClientBuilder.newClient();
    private static final String URL = "http://localhost:8080/boutique/api/Employe";
    
    public static synchronized EmployeClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EmployeClient();
        }
        return INSTANCE;
    }
    
    public List<Employe> lister() {
        Response response = client.target(URL)
                .path("/list")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            liste = response.readEntity(new GenericType<List<Employe>>(){});
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return liste;
    }
    
    public Employe trouver(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            Integer id_prod = response.readEntity(int.class);
            
            for (Employe emp : liste) {
                if (Objects.equals(emp.getId(), id_prod)) {
                    employe = emp;
                    return employe;
                } else {
                }
            }
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return null;
    }
    
    public int compter() {
        int nombre = 0;
        
        Response response = client.target(URL)
                .path("/nombre")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
        }
        else {
            System.out.println(response.getStatus());
        }
        
        return nombre;
    }
    
    public void ajouter(Employe emp) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(emp, MediaType.APPLICATION_JSON_TYPE));
    }
    
    public void modifier(Employe emp) {
        Response response = client.target(URL)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(emp, MediaType.APPLICATION_JSON_TYPE));
                
        if (response.getStatus() == 200) {
            System.out.println("Donnée Employé modifié !");
        }
        else {
            System.out.println(response.getStatus());
        }
        
    }
    
    public void supprimer(Long id) {
        Response response = client.target(URL)
                .path(id.toString())
                .request()
                .delete();
        
        if (response.getStatus() == 200) {
            System.out.println("Employé supprimé !");
        }
        else {
            System.out.println(response.getStatus());
        }
    }
}
