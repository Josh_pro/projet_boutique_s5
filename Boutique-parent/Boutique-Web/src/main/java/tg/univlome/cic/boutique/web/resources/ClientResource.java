package tg.univlome.cic.boutique.web.resources;

import tg.univlome.cic.boutique.entites.Clients;

import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import tg.univlome.cic.boutique.web.services.ClientService;

/**
 *
 * @author Josué K.M. AYIVOR
 */

@Path("Clients")
public class ClientResource {
   
    private ClientService service;
    
    public ClientResource (){
        this.service = ClientService.getInstance();
    }
   
    @GET
    @Path("/list")
    public List<Clients> lister () {
        return this.service.lister();
    }
    
    @GET
    @Path("/{id}")
    public Clients trouver (@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter () {
        return this.service.compter();
    }
     
    @POST
    public void ajouter(Clients c) {
        this.service.ajouter(c);
    }
     
    
    @PUT
    public void modifier(Clients c) {
        this.service.modifier(c);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id) {
        this.service.supprimer(id);
    }
    
}
