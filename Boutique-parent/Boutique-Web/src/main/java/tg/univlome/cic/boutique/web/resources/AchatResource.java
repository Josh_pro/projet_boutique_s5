package tg.univlome.cic.boutique.web.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import tg.univlome.cic.boutique.entites.Achat;
import tg.univlome.cic.boutique.web.services.AchatService;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 * @author Josué K.M. AYIVOR
 * 
 * @@literal Classe Resource de la classe Achat du package entites
 * 
 * @since 5/02/2022
 */

@Path("achat")
public class AchatResource {
    
    private AchatService service;
    
    public AchatResource() {
        this.service = AchatService.getInstance();
    }
    
    @GET
    @Path("/list")
    public List<Achat> lister () {
       return this.service.lister();
    }
    
    @GET
    @Path("/{id}")
    public Achat trouver(@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter() {
        return this.service.compter();
    }
    
    @POST
    public void ajouter(Achat achat) {
        this.service.ajouter(achat);
    }
    
    @PUT
    public void modifier(Achat achat) {
        this.service.modifier(achat);
    }
    
    @DELETE
    @Path("/{id}")
    public void Supprimer (@PathParam("id") long id) {
        this.service.Supprimer(id);
    }
}
