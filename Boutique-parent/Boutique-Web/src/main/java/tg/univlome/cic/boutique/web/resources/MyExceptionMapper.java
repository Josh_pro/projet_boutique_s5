package tg.univlome.cic.boutique.web.resources;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author joshua
 */

@Provider
public class MyExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
         e.printStackTrace();
         return Response
                 .status(Response.Status.CREATED)
                 .type(MediaType.WILDCARD_TYPE)
                 .entity(e.getCause())
                 .build();
    }
    
}
