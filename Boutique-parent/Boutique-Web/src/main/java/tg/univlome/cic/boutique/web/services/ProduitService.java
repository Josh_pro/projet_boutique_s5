package tg.univlome.cic.boutique.web.services;

import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.entites.Produit;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;

/**
 * 
 * @class Classe de service où seront implementé les services de la classe produit
 *
 * @author Josué K.M. AYIVOR
 */

public class ProduitService {
    
    private static List<Produit> liste = new ArrayList<>();
    private static ProduitService INSTANCE;
    
    private ProduitService() {   
        Categorie c1 = new Categorie(1, "Chaussures", "Moccassin, Paire, ...");
        Categorie c2 = new Categorie(2, "Robes", "Ovale, tunique, ...");
        liste.add(new Produit(1l, "Fila", 1000.0, LocalDate.of(2025, 12, 1), c1));
        liste.add(new Produit(2l, "Adidas", 2500.0, LocalDate.of(2030, 12, 1), c1));
        liste.add(new Produit(3l, "Zara", 10000.0, LocalDate.of(2035, 12, 4), c2));
        liste.add(new Produit(4l, "Wax", 7000.0, LocalDate.of(2045, 5, 12), c2));   
    }
            
   public static synchronized ProduitService getInstance() {
       if (INSTANCE == null) {
           INSTANCE = new ProduitService();
       }
       return INSTANCE;
   }
   
   public List<Produit> lister () {
       return liste;
   }
   
   public Produit trouver(long id) {
       
       for (Produit produit : liste) {
           if (produit.getId() == id) {
               return produit;
           }
       }
       return null;
   }
   
   public int compter() {
       return liste.size();
   }
   
   public void ajouter(Produit p) {
       liste.add(p);
   }
   
   public void modifier(Produit p) {
       int i = -1;
       
       for (Produit produit : liste) {
           i++;
           if (produit.equals(p)) {
               break;
           }
       }
       
       if (i >= 0) {
           liste.set(i, p);
       }
   }
   
   public void supprimer (long id){
       liste.remove(id);
   }
}
