package tg.univlome.cic.boutique.web.services;

import tg.univlome.cic.boutique.entites.Clients;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @class ClientService
 *
 * @author Josué K.M. AYIVOR
 */

public class ClientService {
    
    private static List<Clients> liste = new ArrayList<>();
    private static ClientService INSTANCE;
    
    private ClientService() {
        Clients c1 = new Clients("EFEV","ROLCZ",1,"AYIVOR","Kossi Mawulolo", LocalDate.of(1993, 12, 4));
        Clients c2 = new Clients("NECK","AJDCZ",2,"SAQUI","Kofi Joe", LocalDate.of(1992, 2, 1));
        liste.add(c1);
        liste.add(c2);
    }
    
    public static synchronized ClientService getInstance() {
       if (INSTANCE == null) {
           INSTANCE = new ClientService();
       }
       return INSTANCE;
   }
    
    public List<Clients> lister () {
       return liste;
   }
   
   public Clients trouver(long id) {
       
       for (Clients client : liste) {
           if (client.getId() == id) {
               return client;
           }
       }
       return null;
   }
   
   public int compter() {
       return liste.size();
   }
   
   public void ajouter(Clients c) {
       liste.add(c);
   }
   
   public void modifier(Clients c) {
       int i = -1;
       
       for (Clients client : liste) {
           i++;
           if (client.equals(c)) {
               break;
           }
       }
       
       if (i >= 0) {
           liste.set(i, c);
       }
   }
   
   public void supprimer (long id){
       liste.remove(id);
   }
    
}
