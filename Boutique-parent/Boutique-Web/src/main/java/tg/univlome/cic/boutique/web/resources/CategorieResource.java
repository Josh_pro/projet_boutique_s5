package tg.univlome.cic.boutique.web.resources;

import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.web.services.CategorieService;
import java.util.List;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Josué K.M. AYIVOR
 */

@Path("categorie")
public class CategorieResource {
    
    private CategorieService service;
    
    public CategorieResource() {
        this.service = CategorieService.getInstance();
    }
    
    @GET
    @Path("/list")
    public List<Categorie> lister () {
        return this.service.lister();
    }
    
    @GET
    @Path("/{id}")
    public Categorie trouver (@PathParam("id") long id) {
        return this.service.trouver(id);
    }
    
    @GET
    @Path("/nombre")
    public int compter () {
        return this.service.compter();
    }
    
    @POST
    public void ajouter(Categorie cat) {
        this.service.ajouter(cat);
    }
    
    @PUT
    public void modifier(Categorie cat) {
        this.service.modifier(cat);
    }
    
    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id) {
        this.service.supprimer(id);
    }
}
