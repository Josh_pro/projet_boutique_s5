package tg.univlome.cic.boutique.web.services;

import tg.univlome.cic.boutique.entites.Categorie;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @class CategorieService
 *
 * @author Josué K.M. AYIVOR
 */

public class CategorieService {
    
    private static final List<Categorie> liste = new ArrayList<>();
    private static CategorieService INSTANCE;
    
    public CategorieService() {
        Categorie c1 = new Categorie(1, "Chaussures", "Moccassin, Paire, ...");
        Categorie c2 = new Categorie(2, "Robes", "Ovale, tunique, ...");
        Categorie c3 = new Categorie(3, "Chemises", "Carrelé, Courtes, ...");
        Categorie c4 = new Categorie(4, "Pantalons", "Cassé, Design, ...");
        Categorie c5 = new Categorie(5, "Chapeau", "Kepi, Cascette, ...");
            
        liste.add(c1);
        liste.add(c2);
        liste.add(c3);
        liste.add(c4);
        liste.add(c5);
    }
    
    public static synchronized CategorieService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new CategorieService();
        }
        return INSTANCE;
    }
    
     public List<Categorie> lister () {
        return liste;
    }
    
    public Categorie trouver (long id) {
        for (Categorie categorie : liste) {
            if(categorie.getId() == id) {
                return categorie;
            }
        }
        return null;
    }
    
    public int compter () {
        return liste.size();
    }
    
    public void ajouter(Categorie cat) {
        liste.add(cat);     
    }
    
    public void modifier(Categorie cat) {
        int i=-1;
        
        for (Categorie categorie : liste) {
            if(categorie.equals(cat)) {
                break;
            }
        }
        if (i>= 0 ){
            liste.set(i, cat);
        }
    }
    
    public void supprimer(long id) {
        liste.remove(id);
    }
}
