package tg.univlome.cic.boutique.web.services;

import tg.univlome.cic.boutique.entites.Achat;
import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.entites.Clients;
import tg.univlome.cic.boutique.entites.Employe;
import tg.univlome.cic.boutique.entites.Produit;
import tg.univlome.cic.boutique.entites.ProduitAchete;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @class AchatSercive
 *
 * @author Josué K.M. AYIVOR
 */

public class AchatService {
    
    private static final List<ProduitAchete> listPdt_Acht= new ArrayList<>();
    private static final List<Achat> achats = new ArrayList<>();
    private static AchatService INSTANCE;
    
    public AchatService() {
        Clients c1 = new Clients("EFEV","ROLCZ",1,"AYIVOR","Kossi Mawulolo", LocalDate.of(1993, 12, 4));
        Employe e2 = new Employe("BNCE", 2, "KAWOUDJI", "kodjo Marc", LocalDate.of(1993, Month.MARCH, 3));
        
        
        Categorie cat1 = new Categorie(2, "Robes", "Ovale, tunique, ...");
        
        Produit p1 = new Produit(1l, "Fila", 15000.0, LocalDate.of(2025, 12, 1), cat1);
        Produit p2 = new Produit(2l, "Adidas", 2500.0, LocalDate.of(2030, 12, 1), cat1);
        
        listPdt_Acht.add(new ProduitAchete(15, 0.2, p1));
        listPdt_Acht.add(new ProduitAchete(24, 0.2, p2));
        
        Achat a1 = new Achat(1l, LocalDate.of(2022, Month.JANUARY, 20), 0.1, e2, c1, listPdt_Acht);
        achats.add(a1);
    }
      
    public static synchronized AchatService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new AchatService();
        }
        return INSTANCE;
    }
      
    public List<Achat> lister () {
        return achats;
    }
   
    public Achat trouver(long id) {
        
        for (Achat achat : achats) {
            if(achat.getId() == id) {
                return achat;
            }
        }
        return null;
    }
    
    public int compter() {
        return achats.size();
    }
    
    public void ajouter(Achat achat) {
        int i=-1;
        
        for (Achat achat1 : achats) {
            if(achat.equals(achat)) {
                break;
            }
        }
        
        achats.set(i, achat);
    }
    
    public void modifier(Achat achat) {
        int i=-1;
        
        for (Achat achat1 : achats) {
            i++;
            if(achat1.equals(achat)) {
                break;
            }
        }
        if(i>=0) {
            achats.set(i, achat);
        }
    }
    
    public void Supprimer (long id) {
        achats.remove(id);
    }
}
