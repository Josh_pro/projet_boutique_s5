package tg.univlome.cic.boutique.entites;

import java.io.serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @class Achat 
 * 
 * @author Josué K.M. AYIVOR
 * 
 * @Version : 1.0
 * 
 * @Copyright 2022
 */

public class Achat implements serializable{
    private Long id;
    private LocalDate dateAchat;
    private double remise;
    private Employe employe;
    private Clients client;
    private List<ProduitAchete> produit;
    
    //Le constructeur par défaut
    public Achat() {      
    }
    
    
    public Achat(Long id, LocalDate dateAchat, double remise, Employe emp, Clients cl, List<ProduitAchete> listPdt_Acht) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.remise = remise;
        this.employe = emp;
        this.client = cl;
        this.produit = listPdt_Acht;
    }
    
    public double getRemiseTotale() { 
        double R = 0.0;
        double RT = 0.0; // Remise Totale
        for (ProduitAchete produitAchete : produit) {
            R += produitAchete.getRemise();
        }
        RT = R *this.remise;
        return RT ; 
    }
    
    public double getPrixTotale() { 
        double PT = 0.0; //Prix Total
        double PTAR = 0.0; //Prix Total Avec Remise
        for (ProduitAchete produitAchete : produit) {
            PT += produitAchete.getPrixTotale();
        }
        
        PTAR = PT*this.remise;
        return PTAR; 
    }

    //Les accesseurs
    public void setId(long id) { 
        this.id = id; 
    }

    public Long getId() { 
        return id; 
    }

    public void setRemise(double remise) { 
        this.remise = remise; 
    }

    public double getRemise() { 
        return remise; 
    }

    public void setDateAchat(LocalDate dateAchat) { 
        this.dateAchat = dateAchat; 
    }

    public LocalDate getDateAchat() { 
        return dateAchat; 
    }

    public Clients getClient() {
        return client;
    }

    public Employe getEmploye() {
        return employe;
    }
   
    //Methodes de la classe object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); 
    }

    @Override
    public int hashCode() {
        return super.hashCode(); 
    }

    @Override
    public String toString() {
        return super.toString(); 
    }

    public List<ProduitAchete> getProduit() {
        return this.produit;
    }
}
