package tg.univlome.cic.boutique.entites;

import java.io.serializable;

/**
 * @class ProduitAchete
 * 
 * @author Josué K.M. AYIVOR
 * 
 * @Version : 1.0
 * 
 * @Copyright 2022
 */

public class ProduitAchete implements serializable{
    private int id;
    private int quantite;
    private double remise;
    private Produit produit;
    //private Achat achat;

    public ProduitAchete() {
    }
    
    //le Constructeur
    public ProduitAchete(int quantite, double remise, Produit pdt) {
        this.quantite = quantite;
        this.remise = remise;
        this.produit = pdt;
        //this.achat = acht;   
    }
    
    //Les accesseurs
    public double getPrixTotale() { 
        double PSR = this.produit.getPrixUnitaire() * this.quantite;
        double PAR = PSR - (PSR * this.remise);
        return PAR; 
    }

    public void setQuantite(int quantite) { 
        this.quantite = quantite; 
    }

    public int getQuantite() { 
        return quantite; 
    }

    public void setRemise(double remise) { 
        this.remise = remise; 
    }

    public double getRemise() { 
        return remise; 
    }
    
    public Produit getProduit() {
        return this.produit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //les methodes de la classe Object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); 
    }

    @Override
    public int hashCode() {
        return super.hashCode(); 
    }

    @Override
    public String toString() {
        return super.toString(); 
    }
    
}
