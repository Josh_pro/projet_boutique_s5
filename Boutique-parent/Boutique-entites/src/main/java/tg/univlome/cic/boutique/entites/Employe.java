package tg.univlome.cic.boutique.entites;

import java.io.serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @class Employe 
 * 
 * @author Josué K.M. AYIVOR
 * 
 * @Version : 1.0
 * 
 * @Copyright 2022
 */

public class Employe extends Personne{
    private String cnss;

    public Employe() {
        super(0, null, null, null);
    }
    
    public Employe(String cnss, long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cnss = cnss;
    }

    //Les accesseurs
    public void setCnss(String cnss) { this.cnss = cnss; }

    public String getCnss() { return cnss; }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if(!(obj instanceof Employe)) return false;
        
        Employe e1 = (Employe) obj;
        
        
        return getNom().equalsIgnoreCase(e1.getNom()) && 
               getPrenoms().equalsIgnoreCase(e1.getPrenoms()) &&
               cnss.equalsIgnoreCase(e1.getCnss()) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Nom : " +getNom()+ "\nPrenoms : "+getPrenoms()+"\nDate de Naissance : "+getDateNaissance()
                +"\nN_CNSS : "+getCnss();
    }
    
}
