package tg.univlome.cic.boutique.entites;

import java.io.serializable;

/**
 * @class Categotie 
 * 
 * @author Josué K.M. AYIVOR
 * 
 * @Version : 1.0
 * 
 * @Copyright 2022
 */

public class Categorie implements serializable{
    private int id;
    private String libelle;
    private String description;
    
    //Constructeur par défaut
    public Categorie() { }
    
    public Categorie(int id, String lib, String desc) {
        this.id = id;
        this.libelle = lib;
        this.description = desc;
    }

    //Les accesseurs
    public void setId(int id) { this.id = id; }

    public int getId() { return id; }

    public void setLibelle(String libelle) { this.libelle = libelle; }

    public String getLibelle() { return libelle; }

    public void setDescription(String description) { this.description = description; }

    public String getDescription() { return description; }
    
    //Les methodes de la classe object 
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); 
    }

    @Override
    public int hashCode() {
        return super.hashCode(); 
    }

    @Override
    public String toString() {
        return super.toString(); 
    }
    
}
