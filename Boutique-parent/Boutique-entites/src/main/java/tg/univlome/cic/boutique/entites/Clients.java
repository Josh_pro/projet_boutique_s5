package tg.univlome.cic.boutique.entites;

import java.io.serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * @class Client
 * 
 * @author Josué K.M. AYIVOR
 * 
 * @Version : 1.0
 * 
 * @Copyright 2022
 */

public class Clients extends Personne implements serializable{
    private String cin;
    private String carteVisa;

    public Clients() {
        super(0, null, null, null);
    }

    //Le constructeur
    public Clients(String cin, String carteVisa, long id, String nom, String prenoms, LocalDate dateNaissance) {
        super(id, nom, prenoms, dateNaissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }

    //Les accesseurs
    public void setCin(String cin) { 
        this.cin = cin; 
    }

    public String getCin() { 
        return cin; 
    }

    public void setCarteVisa(String carteVisa) { 
        this.carteVisa = carteVisa; 
    }

    public String getCarteVisa() { 
        return carteVisa; 
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if(!(obj instanceof Clients)) return false;
        
        Clients c1 = (Clients) obj;
        
        
        return getNom().equalsIgnoreCase(c1.getNom()) && 
               getPrenoms().equalsIgnoreCase(c1.getPrenoms()) &&
               cin.equalsIgnoreCase(c1.getCin()) &&
               carteVisa.equalsIgnoreCase(c1.getCarteVisa());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Nom : \n" +getNom()+ "\nPrenoms : "+getPrenoms()+"\nDate de Naissance : "+getDateNaissance()
                +"\nCIN : "+getCin()+"\nCarte Visa : "+getCarteVisa();
    }
    
}
